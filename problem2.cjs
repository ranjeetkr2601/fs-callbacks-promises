const fs = require('fs');

function readFileFn(pathToFile){
    return new Promise((resolve, reject) => {
        fs.readFile(pathToFile, 'utf-8', (error, data) => {
            if(error){
                reject(error);
            }
            else{
                resolve(data);
            }
        }); 
    });  
}

function writeFileFn(pathToFile, data){
    return new Promise((resolve, reject) => {
        fs.writeFile(pathToFile, data, (error) => {
            if(error){
                reject(error);
            }
            else {
                resolve(pathToFile);
            }
        });
    });
}

function appendToFilenames(pathToFile){
    return new Promise((resolve, reject) => {
        fs.appendFile('filenames.txt', "\n" + pathToFile, (error) => {
            if(error){
                reject(error);
            } else {
                resolve(`${pathToFile} appended to filenames.txt`);
            }
        });
    }); 
}

function unlinkFiles(...filesToDelete){
    return new Promise((resolve, reject) => {
        filesToDelete = filesToDelete.flat();
        let length = filesToDelete.length;
        if(length > 0){
            let pathToFile = filesToDelete[0];
            fs.unlink(pathToFile, (error) => {
                if(error){
                    reject(error);
                }
                else {
                    console.log(`${pathToFile} Deleted`);
                    filesToDelete.shift();
                    unlinkFiles(filesToDelete)
                    .then((fromRes) => {
                        resolve(fromRes);
                    })
                    .catch((error) => {
                        reject(error);
                    })   
                }
            });
        }
        else {
            resolve("All Deleted");
        }
        
    });
}

function convertToUpperCase(fileToRead, pathToNewFile){
    return new Promise((resolve, reject) => {
        readFileFn(fileToRead)
        .then((data) => {
            console.log(`Read ${fileToRead} Successfully`);
            data = data.toUpperCase();
            return writeFileFn(pathToNewFile, data);
        })
        .then((pathToFile) => {
            console.log(`Write to ${pathToFile} file successful`);
            return writeFileFn('filenames.txt', pathToFile);
        })
        .then(() => {
            resolve(pathToNewFile);
        })
        .catch((error) => {
            reject(error);
        });
    });
}


function convertToLowerCase(fileToRead, pathToNewFile){
    return new Promise((resolve, reject) => {
        readFileFn(fileToRead)
        .then((data) => {
            console.log(`Read ${fileToRead} Successfully`);
            data = data.toLowerCase()
                .split('. ')
                .join('.\n');
            return writeFileFn(pathToNewFile, data);
        })
        .then((pathToFile) => {
            console.log(`Write to ${pathToFile} file successful`);
            return appendToFilenames(pathToFile);
        })
        .then ((filePath) => {
            resolve(filePath);
        })
        .catch((error) => {
            reject(error);
        })
    });
}

function readMultipleFiles(...filesToRead){

    return new Promise((resolve, reject) => {
        filesToRead = filesToRead.flat();
        let length = filesToRead.length;
        let allData = "";
        if(length > 0){
            let fileToRead = filesToRead[0];
            readFileFn(fileToRead)
            .then((data) => {
                filesToRead.shift();
                allData = allData.concat(data);
                readMultipleFiles(filesToRead)
                .then((nextData) => {
                    allData = allData.concat(nextData)
                    resolve(allData);
                })
                .catch((error) => {
                    reject(error);
                });
            })
            .catch((error) => {
                reject(error);
            });
        }
        else {
            resolve(allData);
        }
    })
        
}

function sortDataOfNewFiles(...filesToRead){
    return new Promise((resolve, reject) => {
        readMultipleFiles(filesToRead)
        .then((data) => {
            console.log("Read new files successfully");
            data = data.split(" ").sort().join(" ");
            return writeFileFn('lipsumSorted.txt', data);
        })
        .then((filePath) => {
            console.log(`Write to ${filePath} successful`);
            return appendToFilenames(filePath);
        })
        .then((filePath) => {
            resolve(filePath);
        })
        .catch((error) => {
            reject(error);
        });
    })
}

function deleteFiles(filesToDelete = 'filenames.txt'){
    return new Promise((resolve, reject) => {
        readFileFn(filesToDelete)
        .then((data) => {
            data = data.split('\n');
            unlinkFiles(data)
            .then((fromRes) => {
                resolve(fromRes);
            })
            .catch((error) => {
                reject(error);
            });
        })
        .catch((error) => {
            reject(error);
        });
    });
}

module.exports.convertToUpperCase = convertToUpperCase;
module.exports.convertToLowerCase = convertToLowerCase;
module.exports.sortDataOfNewFiles = sortDataOfNewFiles;
module.exports.deleteFiles = deleteFiles;