const problem2 = require('../problem2.cjs');

problem2.convertToUpperCase('lipsum.txt', 'lipsumUpperCase.txt')
.then((fromRes) => {
    return fromRes;
})
.then((fromRes) => {
    return problem2.convertToLowerCase(fromRes, 'lipsumLowerCase.txt');
})
.then(() => {
    return problem2.sortDataOfNewFiles('lipsumUpperCase.txt', 'lipsumLowerCase.txt')
})
.then(() => {
    return problem2.deleteFiles();
})
.then((fromRes) => {
    console.log(fromRes);
})
.catch((error) => {
    console.log(error);
})