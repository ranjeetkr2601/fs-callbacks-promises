let problem1 = require('../problem1.cjs');

let path = './files';
let noOfFiles = 10;
problem1.createDirectory(path)
.then((fromRes1) => {
    console.log(`Directory Created - ${fromRes1}`);
    return problem1.createFiles(fromRes1, noOfFiles);
})
.then((fromRes2) => {
    problem1.deleteFiles(fromRes2);
})
.catch((fromRej) => {
    console.log(fromRej);
})