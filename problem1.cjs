const fs = require('fs');
const path = require('path');

function createDirectory(folderPath){
    return new Promise((resolve, reject) => {
        fs.access(folderPath, (error) => {
            if(error) {
              fs.mkdir(folderPath, (error) => {
                if (error) {
                  reject(error);
                } else {
                    resolve(folderPath);
                }
              });
            } else {
                resolve(folderPath);
            }
        });
    });   
}

function createFiles(folderPath, numOfFiles = 10){

    return new Promise((resolve, reject) => {
        let allFiles = [];
        if(numOfFiles > 0){
            let randomName = Math.random().toString().substring(2, 10);
            let currentFile = randomName + ".json";
            let pathToFile = path.join(folderPath, currentFile);
            fs.open(pathToFile, 'w', (error) => {
                if(error){
                    reject(error);
                }
                else{
                    console.log(`${pathToFile} Created`);
                    allFiles.push(pathToFile);
                    createFiles(folderPath, numOfFiles - 1)
                        .then((fromRes) => {
                            allFiles.push(...fromRes);
                            resolve(allFiles);
                        })
                        .catch((fromRej) => {
                            reject(fromRej);
                        });
                }
            });
        } else {
            resolve(allFiles);
        }
    });
}


function deleteFiles(filesToDelete){
    return new Promise((resolve, reject) => {
        let length = filesToDelete.length;
        if(length > 0){
            let fileToDelete = filesToDelete[0];
            fs.unlink(fileToDelete, (error) => {
                if(error){
                    reject(error);
                } else {
                    console.log(`${filesToDelete.shift()} deleted`);
                    deleteFiles(filesToDelete)
                        .then((fromRes) => {
                            resolve(fromRes);
                        })
                        .catch((fromRej) => {
                            reject(fromRej);
                        })
                }
            });
        } else {
            resolve("");
        }
    });
}

module.exports.createDirectory = createDirectory;
module.exports.createFiles = createFiles;
module.exports.deleteFiles = deleteFiles;